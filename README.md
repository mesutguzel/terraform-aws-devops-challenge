# Terraform AWS Ubuntu EC2 Instance

Deploy an AWS EC2 Instance running Ubuntu using Terraform

To update the version of Ubuntu, just update the **ami** line in the **linux-vm-main.tf** file, with a variable from the **ubuntu-versions.tf** file.


